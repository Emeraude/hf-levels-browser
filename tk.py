#!/usr/bin/env python3

from PIL import Image, ImageTk
import tkinter as tk
import collections
import sys
import os
import yaml
import random
from cache import Cache
from links import Links
from logger import Logger
from bindings import Bindings

class Fun:
    def __init__(self, fun, *params):
        self.fun = fun
        self.params = params

    def __call__(self):
        self.fun(*self.params)

class App(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)
        self.cache = Cache(config)
        self.geometry(str(config["display"]["window"]["width"])
                      + 'x'
                      + str(config["display"]["window"]["height"]))
        self.title('Level browser')
        self.call('wm', 'iconphoto', self._w, tk.PhotoImage(file='./assets/icon.png'))
        self.frame = None
        self.switchFrame(SelectPage)

    def switchFrame(self, frameClass, *args):
        if self.frame is not None:
            self.frame.destroy()
        newFrame = frameClass(self, *args)
        self.frame = newFrame

class Level(tk.Frame):
    def __init__(self, master, game):
        tk.Frame.__init__(self, master)
        self.game = game
        lvl = self.game["begin"]
        self.cache = master.cache
        self.links = Links(lvl, config, self.game)
        self.window = master
        self.canvas = tk.Canvas(self, width=420, height=500)
        self.canvas.pack(side = tk.LEFT, expand=tk.YES, fill=tk.BOTH)
        self.canvas.bind('<Configure>', self.resize)
        self.tkImage = None
        self.imgSize = (420, 500)
        fm = tk.Frame(self)
        fm.pack(side = tk.BOTTOM, fill=tk.BOTH)
        self.textStatus = tk.Label(fm, fg="black")
        self.textStatus.pack(side = tk.LEFT)

        self.bindings = Bindings(game)

        self.buttons = {}
        for link in ['next', 'back', 'left', 'right']:
            callback = Fun(self.followLink, link)
            self.buttons[link] = tk.Button(self, text="", command=callback)
            self.buttons[link].pack()

        self.updateButtons()

        self.unbindButton = tk.Button(self, text="Unbind all", command=self.triggerBindings)
        self.unbindButton.pack()
        self.binded = False
        self.triggerBindings()

        self.menuButton = tk.Button(self, text="Back to menu", command=lambda: master.switchFrame(SelectPage)).pack()

        fm = tk.Frame(self)
        fm.pack()
        tk.Label(fm, text="Goto").pack(side=tk.LEFT)
        self.gotoEntry = tk.Entry(fm)
        self.gotoEntry.bind("<Return>", self.goto)
        self.gotoEntry.pack(side=tk.LEFT)
        b = tk.Button(fm, text="Go", command= lambda: self.goto(self.gotoEntry)).pack(side=tk.LEFT)
        self.loadImage()
        self.pack(expand=tk.YES, fill=tk.BOTH)

    def destroy(self):
        tk.Frame.destroy(self)
        self.bindings.removeAll()

    def resize(self, event):
        wratio = float(event.width) / 420.0
        hratio = float(event.height) / 500.0
        if hratio > wratio:
            self.imgSize = (event.width, int(500.0 * wratio))
        else:
            self.imgSize = (int(420.0 * hratio), event.height)
        self.loadImage()

    def triggerBindings(self):
        if self.binded:
            self.unbindButton.config(text="Bind all")
            self.bindings.removeAll()
        else:
            self.bindings.add(config['bindings']['next'], self.followLink, ['next'])
            self.bindings.add(config['bindings']['back'], self.followLink, ['back'])
            self.bindings.add(config['bindings']['left'], self.followLink, ['left'])
            self.bindings.add(config['bindings']['right'], self.followLink, ['right'])
            self.bindings.add(config['bindings']['unbind'], self.triggerBindings)
            if 'goto' in config['bindings'] and config['bindings']['goto'] != None:
                self.bindings.add(config['bindings']['goto'], self.gotoFocus)
            self.unbindButton.config(text="Unbind all")
        self.binded = not self.binded

    def goto(self, entry):
        self.links.goto(self.gotoEntry.get())
        self.gotoEntry.delete(0, 'end')
        self.focus()
        self.loadImage()
        self.updateButtons()

    def gotoFocus(self):
        self.bindings.remove(config['bindings']['goto'])
        self.window.attributes('-topmost', True)
        self.window.attributes('-topmost', False)
        self.window.focus_force()
        self.gotoEntry.focus()
        self.window.after(500, lambda: self.bindings.add(config['bindings']['goto'], self.gotoFocus))

    def updateButtons(self):
        for link in ['next', 'back', 'left', 'right']:
            self.buttons[link].config(text=config['bindings'][link]
                                      + " -> "
                                      + str(self.links.get(link)))

    def followLink(self, direction):
        if self.links.follow(direction):
            self.loadImage()
            self.updateButtons()
            self.cache.preload(self.game, self.links.getAll())
        else:
            Logger.warning("Warning: There is no link \"" + direction + "\"")
            self.textStatus.config(text="Warning: There is no link \"" + direction + "\"",
                                   fg="red")

    def loadImage(self):
        try:
            self.textStatus.config(text="Loading level " + self.links.getCurrent() + "...",
                                   fg="grey")
            self.tkImage = ImageTk.PhotoImage(self.cache.get(self.game,
                                                             self.links.getCurrent())
                                              .resize(self.imgSize, Image.LANCZOS))
            self.canvas.create_image(0, 0, image = self.tkImage, anchor = "nw")
            self.textStatus.config(text="")
        except:
            Logger.warning("Unable to load level " + self.links.getCurrent())
            self.textStatus.config(text="Unable to load level " + self.links.getCurrent(),
                                   fg="red")
            self.links.historyPop()
        finally:
            self.links.update()

class SelectPage(tk.Frame):
    def __init__(self, master):
        tk.Frame.__init__(self, master)
        self.cache = master.cache
        self.games = {}
        for game in os.listdir('./games'):
            if game[-4:] == ".yml":
                self.games[game[:-4]] = yaml.safe_load(open("./games/" + game))
        self.games = collections.OrderedDict(sorted(self.games.items()))
        i = 0
        for k, game in self.games.items():
            self.cache.preload(game, [game["begin"]])
            frame = tk.Frame(self)
            frame.grid(row = int(i / config["display"]["menu"]["cols"]),
                       column = int(i % config["display"]["menu"]["cols"]),
                       sticky="N")
            callback = Fun(master.switchFrame, Level, game)
            imgPath = game["display"]["image"]
            if type(imgPath) is list:
                imgPath = random.choice(imgPath)
            img = self.loadImage(imgPath)
            if img is None:
                Logger.error("Unable to load image for game " + game["display"]["name"])
            else:
                label = tk.Label(frame, image=img)
                if "incomplete" in game\
                   and game["incomplete"] == True:
                    label.config(text="incomplete", fg="red", compound=tk.CENTER)
                label.image = img
                label.pack()
            tk.Button(frame, text=game["display"]["name"],
                      command=callback,
                      wraplength=config["display"]["menu"]["label_width"]).pack()
            i += 1
        self.pack()

    def loadOneImage(self, path):
        try:
            hw = config["display"]["menu"]["image_size"]
            img = Image.open(path).resize((hw, hw), Image.LANCZOS)
            return ImageTk.PhotoImage(img)
        except:
            return None

    def loadImage(self, path):
        img = self.loadOneImage(path)
        if img is None:
            Logger.warning("Unable to load image at path `" + path +  "`. Loading default one.")
            img = self.loadOneImage("./assets/games/default.png")
        return img

if __name__ == "__main__":
    config = yaml.safe_load(open("./config.yml"))
    app = App()
    app.mainloop()
