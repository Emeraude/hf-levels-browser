#!/usr/bin/env python3

class Links:
    def __init__(self, level, config, game):
        self.config = config
        self.game = game
        self.current = level
        self.links = {
            "next": None,
            "back": None,
            "left": None,
            "right": None
        }
        self.history = []
        self.update()

    def getAll(self):
        return self.links

    def get(self, direction):
        return self.links[direction]

    def getCurrent(self):
        return self.current

    def historyPop(self):
        try:
            self.current = self.history[-1]
            return self.history.pop()
        except:
            return None

    def update(self):
        if 'links' in self.game and self.current in self.game['links']\
           and 'next' in self.game['links'][self.current]:
            self.links['next'] = self.game['links'][self.current]['next']
        else:
            split = self.current.rfind('.')
            if (split == -1):
                self.links['next'] = str(int(self.current) + 1)
            else:
                self.links['next'] = self.current[:split] + '.' + str(int(self.current[(split + 1):]) + 1)
        if len(self.history) > 0:
            self.links['back'] = self.history[-1]
        else:
            self.links['back'] = None

        for direction in ['left', 'right']:
            if ('links' in self.game and self.current in self.game['links']):
                if direction in self.game['links'][self.current]:
                    self.links[direction] = self.game['links'][self.current][direction]
                else:
                    self.links[direction] = self.game['links'][self.current]
            else:
                self.links[direction] = None

    def goto(self, lvl):
        self.history.append(self.current)
        self.current = lvl
        self.update()

    def follow(self, direction):
        if direction in self.links and self.links[direction] != None:
            if (direction != "back"):
                self.history.append(self.current)
            else:
                self.history.pop()
            self.current = self.links[direction]
            return True
        else:
            return False

