#!/usr/bin/env python3

import keyboard
from logger import Logger

class Bindings:
    def __init__(self, game):
        self.isAvailable = True
        self.bindings = []
        self.gameBindings = {
            "solo": ["left", "right", "up", "down", "ctrl", "p", "c"],
            "multi": ["r", "d", "f", "g", "a", "enter"],
            "misc": ["m", "b"],
            "game specific": []
        }
        if "keys" in game:
            self.gameBindings["game specific"] = game["keys"]

    def __exit__(self):
        self.removeAll()

    def add(self, key, fun, args = None):
        key = key.lower()
        if self.isAvailable:
            try:
                for cat in self.gameBindings:
                    if key in self.gameBindings[cat]:
                        Logger.warning('The key "' + key + '" is already used in game (' + cat + ')')
                keyboard.add_hotkey(key, fun, args)
                self.bindings.append(key)
            except ImportError as e:
                Logger.error('Unable to bind "' + key + '". ' + str(e))
                self.isAvailable = False
            except ValueError as e:
                Logger.error('Unable to bind "' + key + '". ' + str(e))

    def removeAll(self):
        for k in self.bindings:
            self.remove(k)

    def remove(self, key):
        try:
            keyboard.remove_hotkey(key.lower())
        except:
            pass
