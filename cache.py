#!/usr/bin/env python3

import _thread
import requests
import traceback
from io import BytesIO
from PIL import Image
from logger import Logger

class Cache:
    def __init__(self, config):
        self.pilImages = {}
        self.config = config

    def load_one(self, game, lvl):
        gameName = game["display"]["name"]
        if lvl not in self.pilImages[gameName]:
            Logger.notice("Made request for level " + lvl)
            res = requests.get(self.config['hamm4all']['url'] + game["path"] + lvl + ".png",
                               timeout=(self.config['hamm4all']['timeout']['connect'],
                                        self.config['hamm4all']['timeout']['download']))
            if res.status_code == 200:
                gameName = game["display"]["name"]
                self.pilImages[gameName][lvl] = Image.open(BytesIO(res.content))

    def load_async(self, game, lvls):
        if type(lvls) is dict:
            lvls = list(dict((v, k) for k,v in lvls.items()))
        for lvl in lvls:
            if lvl != None and lvl not in self.pilImages:
                _thread.start_new_thread(self.load_one, (game, lvl))

    def preload(self, game, lvls):
        gameName = game["display"]["name"]
        if gameName not in self.pilImages:
            self.pilImages[gameName] = {}
        _thread.start_new_thread(self.load_async, (game, lvls))

    def get(self, game, lvl):
        gameName = game["display"]["name"]
        if gameName not in self.pilImages:
            self.pilImages[gameName] = {}
        if lvl in self.pilImages[gameName]:
            Logger.notice("Retrieving level " + lvl + " from cache.")
            return self.pilImages[gameName][lvl]
        else:
            self.load_one(game, lvl)
            return self.pilImages[gameName][lvl]
