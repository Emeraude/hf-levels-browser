#!/usr/bin/env python3

import sys
try:
    from colorama import init, Fore, Back, Style
    color = True
    init()
except:
    color = False

def printerr(data):
    print(data, file=sys.stderr)

class Logger:
    def notice(data):
        if color:
            printerr("[" + Back.GREEN + Style.BRIGHT + "NOTICE" + Style.RESET_ALL + "] " + data)
        else:
            printerr("[NOTICE] " + data)

    def warning(data):
        if color:
            printerr("[" + Back.YELLOW + Style.BRIGHT + "WARNING" + Style.RESET_ALL + "] " + data)
        else:
            printerr("[WARNING] " + data)

    def error(data):
        if color:
            printerr("[" + Fore.RED + Style.BRIGHT + "ERROR" + Style.RESET_ALL + "] " + data)
        else:
            printerr("[ERROR] " + data)

    def fatalError(data):
        if color:
            printerr("[" + Fore.RED + Style.BRIGHT + "FATAL ERROR" + Style.RESET_ALL + "] " + data)
        else:
            printerr("[FATAL ERROR] " + data)
