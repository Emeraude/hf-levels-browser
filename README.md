# Hammerfest levels browser

The goal of this project is to provide a standalone tool that display the current level and catch key bindings to change the currently selected level.  
The defaults bindings are the following:

- **N** *next* go to next level
- **B** *back* go to previously selected level
- **R** *right* go to dimension to the right
- **L** *left* go to dimension to the left
- **O** *goto level* put window on top and prompt for a level name
- **U** *unbind* disable all the bindings

Note that if there is only one dimension in a given level, **R** and **L** are acting identical.

## Usage
### Linux

`sudo ./tk.py`

Since this tool is acting as a keylogger, you have to run it as superuser.

### Windows

It should work correctly, but I didn't test it.

## Dependencies

- python3
- tk
- pillow >= 5.4
- keyboard
- colorama (optional)
